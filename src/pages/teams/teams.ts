import { TeamModel } from './../../providers/teams/models/team.model';
import { TeamsProvider } from './../../providers/teams/teams.provider';
import { CompetitionsProvider } from '../../providers/competitions/competitions.provider';
import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController } from 'ionic-angular';
import { TabsPage } from '../tabs/tabs';
import { CompetitionModel } from '../../providers/competitions/models/competition.model';
import { MatchModel } from '../../providers/matches/models/match.model';
import { MatchesProvider } from '../../providers/matches/matches.provider';

@IonicPage()
@Component({
  selector: 'page-teams',
  templateUrl: 'teams.html',
})
export class TeamsPage implements OnInit {
  scrollBarHorizontal = (window.innerWidth < 1200);
  competitions: CompetitionModel[] = [];
  teams: TeamModel[] = [];
  teamSelected = false;
  competitionSelected: CompetitionModel = new CompetitionModel();
  loading;
  title = 'Competiciones';
  tableCompetition = false;
  table = [];
  tablestyle = 'bootstrap';

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private competitionsProvider: CompetitionsProvider,
    private teamsProvider: TeamsProvider,
    private matchesProvider: MatchesProvider,
    public loadingCtrl: LoadingController,
  ) { }
  
  ngOnInit() {
    if (this.navCtrl.id !== 't0-0') {
      this.navCtrl.setRoot(TabsPage)
    }
    this.getCompetitions();
    window.onresize = () => {
      this.scrollBarHorizontal = (window.innerWidth < 1200);
    };
  }

  getCompetitions() {
    this.competitionsProvider.getCompetitions()
      .then((competitionsResponse: CompetitionModel[]) => {
        this.competitions = competitionsResponse;
      })
  }

  getTableOfCompetition() {
    this.competitionsProvider.getTablesFromCompetition(
      this.competitionSelected.country,
      this.competitionSelected.code
    ).then((res: any) => {
      this.tableCompetition = true
      console.log(res);
      this.table = res;
    })
  }

  selectCompetition(competition: CompetitionModel) {
    this.getTeams(competition)
    this.teamSelected = true;
    this.competitionSelected = competition;
    this.title = 'Equipos'
  }

  getTeams(competition: CompetitionModel) {
    this.teamsProvider.getTeams(competition.country)
      .then((teamsResponse: TeamModel[]) => this.teams = teamsResponse)
      .catch((error) => console.error(error))
  }

  selectTeam(team: TeamModel) {
    this.presentLoadingDefault();
    this.matchesProvider.getMatches(
      this.competitionSelected.country,
      team.code
    )
      .subscribe((res: MatchModel[]) => {
        this.loading.dismiss();
        this.navCtrl.push('MatchesPage', { matches: res });
      })
  }

  presentLoadingDefault() {
    this.loading = this.loadingCtrl.create({
      content: 'Buscando...'
    });
    this.loading.present();
  }

  backToCompetitions() {
    this.title = 'Competiciones';
    this.teamSelected = false;
  }

}
