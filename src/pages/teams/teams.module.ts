import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TeamsPage } from './teams';
import { SharedModule } from '../../shared/shared.module';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';

@NgModule({
  declarations: [
    TeamsPage,
  ],
  imports: [
    NgxDatatableModule,
    SharedModule,
    IonicPageModule.forChild(TeamsPage),
  ],
})
export class TeamsPageModule {}
