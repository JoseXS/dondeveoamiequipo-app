import { Component } from '@angular/core';

@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {

  tab1Root = 'TeamsPage';
  tab2Root = 'SearchPage';

  constructor() {

  }
}
