import { MatchesResponse } from './../../providers/matches/models/matchs-response.interface';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController } from 'ionic-angular';
import { MatchesProvider } from '../../providers/matches/matches.provider';
import { TabsPage } from '../tabs/tabs';

@IonicPage()
@Component({
  selector: 'page-search',
  templateUrl: 'search.html',
})
export class SearchPage {
  country = 'España';
  team = 'Barcelona';
  loading;
  competitions = ['España', 'Italia', 'Inglaterra', 'Francia'];

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private matchesProvider: MatchesProvider,
    public loadingCtrl: LoadingController,
  ) {
    if (this.navCtrl.id !== 't0-1') {
      this.navCtrl.setRoot(TabsPage)
    }
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SearchPage');
  }

  searchMatch() {

    this.presentLoadingDefault();
    this.country = this.traduceCountry(this.country);
    this.matchesProvider.getMatches(
      this.country,
      this.team
    )
      .subscribe((matchsResponse: MatchesResponse) => {
        this.loading.dismiss();
        this.navCtrl.push('MatchesPage', { matches: matchsResponse });
      })
  }

  traduceCountry(country) {
    switch (country) {
      case 'España':
        return 'spain'
      case 'Italia':
        return 'italy'
      case 'Inglaterra':
        return 'england'
      default:
        break;
    }
  }

  presentLoadingDefault() {
    this.loading = this.loadingCtrl.create({
      content: 'Buscando...'
    });
    this.loading.present();
  }

}
