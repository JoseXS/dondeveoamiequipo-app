import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MatchesPage } from './matches';
import { MatchesProvider } from '../../providers/matches/matches.provider';

@NgModule({
  declarations: [
    MatchesPage
  ],
  imports: [
    IonicPageModule.forChild(MatchesPage),
  ],
  providers: [
    MatchesProvider
  ]
})
export class MatchesPageModule {}
