import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { MatchesResponse } from './../../providers/matches/models/matchs-response.interface';
import { MatchModel } from '../../providers/matches/models/match.model';
import { TabsPage } from '../tabs/tabs';

@IonicPage()
@Component({
  selector: 'page-matches',
  templateUrl: 'matches.html',
})
export class MatchesPage implements OnInit {
  matchesPlayed: MatchModel[] = [];
  matchesNotPlayed: MatchModel[] = [];
  title = 'Partidos';
  itemExpandHeight: number = 0;

  constructor(
    public navCtrl: NavController,
    public params: NavParams,
  ) { }

  ngOnInit() {
    let matches = this.params.get('matches');
    if (matches === undefined) {
      this.navCtrl.setRoot(TabsPage);
      return;
    }
    this.getMatches(matches);
  }

  getMatches(matches: MatchesResponse) {
    this.matchesNotPlayed = matches.matchesNotPlayed
    this.matchesPlayed = matches.matchesPlayed;
    console.log(matches);
    this.matchesNotPlayed.forEach((match) => match['expanded'] = false)
    this.matchesPlayed.forEach((match) => match['expanded'] = false)
  }

  expandMatchNotPlayed(matchExpand) {
    this.matchesNotPlayed.map((match) => {
      if (matchExpand == match) {
        match['expanded'] = !match['expanded'];
        this.itemExpandHeight = 100;
      } else {
        match['expanded'] = false;
        this.itemExpandHeight = 0;
      }
      return match;
    });
  }

  expandMatchPlayed(matchExpand) {
    this.matchesPlayed.map((match) => {
      if (matchExpand == match) {
        match['expanded'] = !match['expanded'];
        this.itemExpandHeight = 100;
      } else {
        match['expanded'] = false;
        this.itemExpandHeight = 0;
      }
      return match;
    });
  }

}


