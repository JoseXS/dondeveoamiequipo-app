import { environment } from '../../envinronments/environment';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class TeamsProvider {
    url = environment.url;
    constructor(private http: HttpClient) { }

    getTeams(country: string) {
        return this.http.get(`${this.url}/teams/${country}`).toPromise()
    }

}