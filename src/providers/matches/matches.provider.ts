import { environment } from './../../envinronments/environment';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class MatchesProvider {
    url = environment.url;
    constructor(private http: HttpClient) { }

    getMatches(country: string, team: string) {
        team = this.slugify(team.toLowerCase());
        return this.http.get(`${this.url}/matches/${country}/${team}`)
    }

    slugify(text) {
        return text.toString().toLowerCase()
          .replace(/\s+/g, '-')           // Replace spaces with -
          .replace(/[^\w\-]+/g, '')       // Remove all non-word chars
          .replace(/\-\-+/g, '-')         // Replace multiple - with single -
          .replace(/^-+/, '')             // Trim - from start of text
          .replace(/-+$/, '');            // Trim - from end of text
      }
    
}