import { MatchModel } from './match.model';

export interface MatchesResponse {
    matchesNotPlayed: MatchModel[],
    matchesPlayed: MatchModel[]
}