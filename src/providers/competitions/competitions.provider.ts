import { environment } from '../../envinronments/environment';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class CompetitionsProvider {
    url = environment.url;
    constructor(private http: HttpClient) { }

    getCompetitions() {
        return this.http.get(`${this.url}/competitions`).toPromise()
    }

    getTablesFromCompetition(country: string, competition: string) {
        return this.http.get(`${this.url}/competitions/table/${country}/${competition}`).toPromise() 
    }
    
}