import { environment } from './../../envinronments/environment';
import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'image'
})

export class ImagePipe implements PipeTransform {
    transform(value: any, type: string, type2?: string): any {
        switch (type) {
            case 'country':
                return `${environment.url}/imageCountry/${value}`
            case 'team':
                return `${environment.url}/imageTeam/${type2}/${value}`
            default:
                break;
        }
    }
}