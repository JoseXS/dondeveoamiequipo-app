import { NgModule } from '@angular/core';
import { ImagePipe } from './pipes/images.pipe';


@NgModule({
    imports: [],
    exports: [ImagePipe],
    declarations: [ImagePipe],
    providers: [],
})
export class SharedModule { }
